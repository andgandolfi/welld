FROM maven:3.6-jdk-13

WORKDIR /usr/src
COPY . .
RUN mvn package -DskipTests

CMD ["java", "-jar", "target/welld-service-0.1.0.jar"]
