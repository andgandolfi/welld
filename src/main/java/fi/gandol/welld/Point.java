package fi.gandol.welld;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Objects;

/**
 * 2-dimensional Point class.
 * This class accept only integer point values, because using floating point makes it difficult
 * to check whether three or more points are on the same line (epsilon coefficient for the error would be needed).
 * I'm not creating getters and setters for such a simple case since they would just clutter the code.
 */
public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getDistance(Point p) {
        return getDistance(this, p);
    }

    public static double getDistance(Pair<Point, Point> points) {
        return getDistance(points.getLeft(), points.getRight());
    }

    public static double getDistance(Point a, Point b) {
        return Math.sqrt(
                Math.pow(Math.abs(b.x - a.x), 2) +
                Math.pow(Math.abs(b.y - a.y), 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}
