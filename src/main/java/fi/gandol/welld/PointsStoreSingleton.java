package fi.gandol.welld;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

import static fi.gandol.welld.Helpers.pairToList;

/**
 * Singleton class used to store the points set in memory.
 *
 * There are many ways this feature could have been implemented based on the specs.
 * I just picked the simplest solution maintaining the thread-safety.
 */
public class PointsStoreSingleton {
    private static volatile PointsStoreSingleton instance;
    private static final Object lock = new Object();

    private final Set<Point> points;

    private PointsStoreSingleton() {
        points = Collections.synchronizedSet(new HashSet<>());
    }

    public static PointsStoreSingleton getInstance() {
        PointsStoreSingleton result = instance;
        if (result == null) {
            synchronized (lock) {
                result = instance;
                if (result == null) {
                    instance = result = new PointsStoreSingleton();
                }
            }
        }
        return result;
    }

    public void addPoint(int x, int y) {
        points.add(new Point(x, y));
    }

    public void addPoint(Point p) {
        points.add(p);
    }

    public Set<Point> getPoints() {
        return points;
    }

    public void removeAllPoints() {
        points.clear();
    }

    protected Map<Line, Set<Point>> getLinesWithPoints() {
        Map<Line, Set<Point>> linesPoints = new HashMap<>();
        List<Point> pointsList;

        // Synchronizing block while building a list out of the points' set
        synchronized (points) {
            pointsList = new ArrayList<>(points);
        }

        int n = pointsList.size();
        for (int i = 0; i < n; i++) {   // Double for loop to generate any possible line between 2 points
            for (int j = i + 1; j < n; j++) {
                Point p1 = pointsList.get(i);
                Point p2 = pointsList.get(j);
                Line l = new Line(p1, p2);
                if (linesPoints.containsKey(l)) {
                    linesPoints.get(l).add(p2);
                } else {
                    Set<Point> initialPoints = new HashSet<>();
                    initialPoints.add(p1);
                    initialPoints.add(p2);
                    linesPoints.put(l, initialPoints);
                }
            }
        }

        return linesPoints;
    }

    public List<List<Point>> getLongestSegmentsWithAtLeastNPoints(int nPoints) {
        double longest = 0;
        List<List<Point>> results = new ArrayList<>();

        for (Map.Entry<Line, Set<Point>> entry : getLinesWithPoints().entrySet()) {
            if (entry.getValue().size() < nPoints) {
                continue;
            }
            Pair<Point, Point> segmentExtremes = Line.getSegmentExtremePoints(entry.getValue());
            double length = Point.getDistance(segmentExtremes);
            if (length == longest) {
                results.add(pairToList(segmentExtremes));
            } else if (length > longest) {
                results.clear();
                results.add(pairToList(segmentExtremes));
                longest = length;
            }
        }

        return results;
    }
}
