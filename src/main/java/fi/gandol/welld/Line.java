package fi.gandol.welld;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collection;
import java.util.Objects;

public class Line {
    // Line coefficients (a*x+b*y=c)
    private int a;
    private int b;
    private int c;

    public Line(Point p1, Point p2) {
        a = p2.y - p1.y;
        b = p1.x - p2.x;

        if (a == 0 && b == 0) {   // Coincident points
            c = 0;
            return;
        }
        if (a == 0) {   // Horizontal line
            b = 1;
            c = p1.y;
            return;
        }
        if (b == 0) {   // Vertical line
            a = 1;
            c = p1.x;
            return;
        }
        // Other lines
        if (a < 0) {    // Normalize coefficients' sign
            a *= -1;
            b *= -1;
        }
        int div = gcd(a, b);
        a /= div;
        b /= div;
        c = a * p1.x + b * p1.y;
    }

    // This function asserts that all the points passed as arguments are on the same line
    public static Pair<Point, Point> getSegmentExtremePoints(Collection<Point> points) {
        if (points == null || points.isEmpty()) {
            return null;
        }

        Point min = null, max = null;
        for (Point p : points) {
            if (min == null) {
                min = max = p;
                continue;
            }
            if (p.x == min.x) {
                if (p.y < min.y) {
                    min = p;
                } else if (p.y > max.y) {
                    max = p;
                }
            } else if (p.x < min.x) {
                min = p;
            } else if (p.x > max.x) {
                max = p;
            }
        }

        return new ImmutablePair<>(min, max);
    }

    static int gcd(int a,int b)
    {
        a = Math.abs(a);
        b = Math.abs(b);
        int min = Math.min(a, b);
        int max = a + b - min;
        int div = min;
        for(int i = 1; i < min; div = min / ++i) {
            if(min % div == 0 && max % div == 0) {
                return div;
            }
        }
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        return a == line.a && b == line.b && c == line.c;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c);
    }

    @Override
    public String toString() {
        return String.format("%d*x" + (b >= 0 ? "+" : "") +"%d*y=%d", a, b, c);
    }
}
