package fi.gandol.welld;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class containing the REST API endpoints
 */
@RestController
public class PointsController {
    @RequestMapping(value = "/point", method = RequestMethod.POST)
    public void addPoint(@RequestBody Point point) {
        PointsStoreSingleton pointsStore = PointsStoreSingleton.getInstance();
        pointsStore.addPoint(point);
    }

    @RequestMapping(value = "/space")
    @ResponseBody
    public Map<String, Set<Point>> getSpace() {
        PointsStoreSingleton pointsStore = PointsStoreSingleton.getInstance();
        // Here I'm returning a JSON object:
        //   {"points":[...]}
        // The specs doc indicate that the response is an array, but that's not
        // a valid JSON response.
        Map<String, Set<Point>> response = new HashMap<>();
        response.put("points", pointsStore.getPoints());
        return response;
    }

    @RequestMapping(value = "/space", method = RequestMethod.DELETE)
    public void cleanSpace() {
        PointsStoreSingleton pointsStore = PointsStoreSingleton.getInstance();
        pointsStore.removeAllPoints();
    }

    @RequestMapping(value = "/lines/{nPoints}")
    @ResponseBody
    public Map<String, List<List<Point>>> getLongestSegment(@PathVariable int nPoints) {
        PointsStoreSingleton pointsStore = PointsStoreSingleton.getInstance();
        Map<String, List<List<Point>>> response = new HashMap<>();
        response.put("segments", pointsStore.getLongestSegmentsWithAtLeastNPoints(nPoints));
        return response;
    }
}
