package fi.gandol.welld;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

public class Helpers {
    // Helper used to convert a pair into a list
    public static <T> List<T> pairToList(Pair<T, T> pair) {
        List<T> result = new ArrayList<T>(2);
        result.add(pair.getLeft());
        result.add(pair.getRight());
        return result;
    }
}
