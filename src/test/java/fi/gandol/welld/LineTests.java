package fi.gandol.welld;

import org.junit.Test;

public class LineTests {
    @Test
    public void testReversedLine() {
        Point p = new Point(4, 4);
        Point q = new Point(8, 12);

        Line l = new Line(p, q);
        Line lRev = new Line(q, p);

        System.out.println(l);
        assert l.equals(lRev);
    }

    @Test
    public void testVerticalLine() {
        Point p = new Point(4, 4);
        Point q = new Point(4, 12);

        Line l = new Line(p, q);

        System.out.println(l);
    }

    @Test
    public void testHorizontalLine() {
        Point p = new Point(4, 4);
        Point q = new Point(10, 4);

        Line l = new Line(p, q);

        System.out.println(l);
    }

    @Test
    public void testCoincidentPoints() {
        Point p = new Point(4, 4);
        Point q = new Point(4, 4);

        Line l = new Line(p, q);

        System.out.println(l);
    }

    @Test
    public void testSegmentLength() {
        Point p = new Point(4, 4);
        Point q = new Point(-6, -6);

        assert Point.getDistance(p, q) == Math.sqrt(2) * 10;
    }
}
