package fi.gandol.welld;

import org.junit.Test;

import java.util.Map;
import java.util.Set;

public class PointStoreTests {
    @Test
    public void testGetLinesWithPoints() {
        PointsStoreSingleton store = PointsStoreSingleton.getInstance();
        store.addPoint(5, 5);
        store.addPoint(-5, 5);
        store.addPoint(-5, -5);
        store.addPoint(5, -5);
        store.addPoint(0, 0);
        store.addPoint(2, 2);

        Map<Line, Set<Point>> linesMap = store.getLinesWithPoints();
        for (Map.Entry<Line, Set<Point>> entry : linesMap.entrySet()) {
            System.out.println(entry.getKey());
            for (Point point: entry.getValue()) {
                System.out.println(String.format("    %s", point));
            }
        }
        System.out.println(String.format("      %s",
                store.getLongestSegmentsWithAtLeastNPoints(4)));
    }

    @Test
    public void testGetLinesWithOverlappingPoints() {
        PointsStoreSingleton store = PointsStoreSingleton.getInstance();
        store.addPoint(5, 5);
        store.addPoint(5, 5);
        store.addPoint(5, 5);
        store.addPoint(5, 5);
        store.addPoint(0, 0);
        store.addPoint(2, 2);

        Map<Line, Set<Point>> linesMap = store.getLinesWithPoints();
        for (Map.Entry<Line, Set<Point>> entry : linesMap.entrySet()) {
            System.out.println(entry.getKey());
            for (Point point: entry.getValue()) {
                System.out.println(String.format("    %s", point));
            }
        }
        System.out.println(String.format("      %s",
                store.getLongestSegmentsWithAtLeastNPoints(3)));
    }
}
