# How to run the project

## Using docker

* `cd` into the project folder
* Build the image `docker build -t gandolfi_welld .`
* Run the container `docker run --rm -p 8080:8080 gandolfi_welld`
* Access the REST API on `http://localhost:8080/`

## Using maven and java installed on the system (I used maven 3.6 and java 13)

* `cd` into the project folder
* Run `mvn spring-boot:run`
* Access the REST API on `http://localhost:8080/`

## Use the API deployed on my server

* I've deployed the docker image on my kubernetes cluster; the REST API can be reached at `https://welld.gandol.fi/`. Note that I'm using this server for many other things, so the API might feel slow or even down(SLA 1% :).
